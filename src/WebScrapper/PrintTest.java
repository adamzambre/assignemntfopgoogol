
package WebScrapper;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PrintTest {

    
    
    public static void main(String[] args) {

        try{
        Scanner websites = new Scanner(new FileInputStream("1000WebList.txt"));//1000WebList.txt
        PrintWriter weboutput = new PrintWriter(new FileOutputStream("Websites2.txt"));//Websites2.txt
        PrintWriter tagsoutput = new PrintWriter(new FileOutputStream("Tags2.txt"));//Tags2.txt
        
while(true){
    try{
        while(websites.hasNextLine()){
        String website = websites.nextLine();
        Document doc = Jsoup.connect("https://" + website).get();
        String title = doc.title();
        String keywords = doc.select("meta[name=keywords]").attr("content");
        //String description = doc.select("meta[name=description]").attr("content");
        if(title == null || title.isEmpty()){
            title = "No title found";
        }if(keywords.isEmpty()){
            keywords = "No Keywords Found";
        }/*if(description.isEmpty()){
            description = "No Description Found";
        }*/

weboutput.println(website);
tagsoutput.println("%%"+title + " " + keywords + " " /*+ description*/);
       }
        //list of websites finished
        break;
        
        }catch(IOException e){// website is not found
        String website = websites.nextLine();
        String title = "Data Cannot be Found";
        System.out.println(website);
        weboutput.println(website);
        System.out.println(title);
        tagsoutput.println(title);
        }
}//true loop

        weboutput.close();
        tagsoutput.close();
        
        }catch(IOException e){//file cannot be read
            System.out.println("File Not Found");
        }
        
}
    }


